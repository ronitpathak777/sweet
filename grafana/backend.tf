terraform {
  backend "s3" {
    bucket  = "grafanaronit"
    key     = "new/terraform.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}
