################ VPC ###########################
variable "cidr_block" {
  description = "specify cidr block for vpc"
  type        = string
  default     = "10.0.0.0/18"
}
variable "instance_tenancy" {
  description = "specify tenancy"
  type        = string
  default     = "default"
}
variable "tags" {
  description = "specify name"
  type        = string
  default     = "grafana_vpc"
}
#######################################################
#--------public subnet ----------#
variable "pub_sub_vpc" {
  description = "specify cidr block for vpc"
  type        = string
  default     = "10.0.0.0/23"
}

variable "pub_sub_tags" {
  description = "specify tag"
  type        = string
  default     = "ninja-pub-sub-01"
}
variable "av_zone_pub" {
  default     = ""
  type        = string
  description = "description"
}
variable "public_sub_cidr" {
  description = "specify cidr block for vpc"
  type        = string
  default     = "10.0.1.0/24"
}

#--------------pvt-subnet------#
variable "private_subnet_cidr" {
  description = "specify cidr block for vpc"
  type        = list(string)
  default     = ["10.0.4.0/23", "10.0.6.0/23"]
}
variable "pvt_subnet_tag" {
  description = "specify tag"
  type        = list(string)
  default     = ["ninja-priv-sub-01", "ninja-priv-sub-02"]
}
variable "av_zone_pri" {
  type        = list(string)
  description = "description"
}
##########igw#########
variable "igw" {
  description = "specify tag"
  type        = string
  default     = "igw-1"
}
variable "vpc_igw" {
  description = "specify tag"
  default     = "vpc_igw"
}
##############rtb############
variable "pub_rtb" {
  description = "route table name"
  type        = string
  default     = "public-rtb"
}
variable "pub_rtb_route" {
  description = "routes"
  type        = string
  default     = "0.0.0.0/0"
}
variable "igw_gateway" {
  description = "igw_gateway"
  default     = "igw_gateway"
}
############### nat-gateway ##############################
variable "nat_id" {
  default     = ""
  description = "description"
}

variable "private_route_table_tag" {
  default     = ""
  description = "description"
}

variable "eip_tag" {
  description = "description"
}


variable "nat_tag" {
  description = "description"
}
######################EC2 ###############################
variable "bastion_instance_type" {
  description = "specify the bastion instance type here"
  type        = string
}
variable "bastion_tags" {
  description = "specify the bastion instance tag here"
  type        = string
}
variable "bastion_subnet_id" {
  description = "specify public subnet id here"
  type        = string
  default     = "bastion"
}
variable "key_name" {
  description = "specify key name here"
  type        = string
  default     = "grafana"
}
variable "prometheus_instance_type" {
  description = "specify prometheus instance types here"
  type        = string
  default = "t3.medium"
}
variable "prometheus_server_tags" {
  description = "specify prometheus server tags here"
  type        = string
  default = "prometheus"
}
variable "grafana_instance_type" {
  description = "specify grafana instance type here"
  type        = string
  default     = "t2-micro"
}
variable "grafana_subnet_id" {
  description = "specify grafana subnet id here"
  type        = list(string)
  default     = ["grafana_subnet"]
}
variable "grafana_server_tags" {
  description = "specify kafka server tags here"
  type        = list(string)
}
variable "prometheus_sg_id" {
  description = "specify zookeeper security group id here"
  type        = string
  default     = "prometheus_sg_id"
}
variable "grafana_sg_id" {
  description = "specify kafka security group id here"
  type        = string
  default     = "grafana_sg_id"
}


############################ security group ##########################
variable "bastion_sg_name" {
   description = "specify the security group name here"
   type = string
}
variable "inbound_ports" {
   description = "specify the inbound ports here"
   type = list (string)
}
variable "ingr_protocol" {
   description = "specify the ingress protocol here"
   type = string
}
variable "ingr_cidr_block" {
   description = "specify the ingress cidr block here"
   type = list (string)
}
variable "prometheus_sg_name" {
  description = "specify the security group name here"
  type        = string
}
variable "prometheus_inbound_ports" {
  description = "specify the inbound ports here"
  type        = list(string)
}
variable "prometheus_ingr_protocol" {
  description = "specify the ingress protocol here"
  type        = string
  default     = "tcp"
}
variable "prometheus_ingr_cidr_block" {
  description = "specify the ingress cidr block here"
  type        = list(string)
  default     = ["0.0.0.0/0"]
}
variable "grafana_sg_name" {
  description = "specify the security group name here"
  type        = string
  default     = "grafana_sg"
}
variable "grafana_inbound_ports" {
  description = "specify the inbound ports here"
  type        = list(string)
}
variable "grafana_ingr_protocol" {
  description = "specify the ingress protocol here"
  type        = string
}
variable "grafana_ingr_cidr_block" {
  description = "specify the ingress cidr block here"
  type        = list(string)
}

#########################launch template ###############################################

variable "grafana" {
  type        = string
  description = "template name"
  default     = "grafana"
}
variable "image_id" {
  type        = string
  description = "launch_ami"
  default     = "grafana"
}
variable "instance_type" {
  type        = string
  description = "instance"
  default     = "t2.micro"
}

variable "key_grafana" {
  type    = string
  default = "grafana"
}
#################### asg ########################
variable "max_size" {
  default = "8"
}
variable "min_size" {
  default = "2"
}
variable "desired_capacity" {
  default = "2"
}
variable "asg_health_check_type" {
  default = "ec2"
}
variable "availability_zones" {
  type    = list(string)
  default = ["us-east-1a", "us-east-1b", "us-east-1c"]
}
variable "grafana_asg" {
  default = "grafana_asg"
}
##########sacle up ############
variable "grafana_asg_scale_up" {
  default = "grafana_sacle"
}
variable "adjustment_type" {
  default = "ChangeInCapacity"
}
variable "scaling_adjustment" {
  default = "1"
}
variable "cooldown" {
  default = "300"
}
variable "policy_type" {
  default = "simple scaling"
}
variable "asg-scale-up-alarm" {
  default = "grafana-scale-up"
}
variable "metric_name" {
  default = "120"
}
variable "threshold_value" {
  default = "30"
}
variable "subnets" {
  type    = list(string)
  default = ["grafana-sub-01", "grafana-sub-02", "grafana-sub-03"]
}

####################### scale down ############
variable "asg-scale-down" {
  default = "grafana_asg_scale_down"
}
variable "adjustment_grafana_type" {
  default = "ChangeInCapacity"
}
variable "grafana_scaling_adjustment" {
  default = "-1"
}
variable "cooldown_period" {
  default = "300"
}
variable "grafana_policy_type" {
  default = "SimpleScaling"
}
variable "asg-scale-down-alarm" {
  default = "asg-scale-down"
}
variable "cloudwatch_metric_name" {
  default = "CPUUtilization"
}
variable "ec2_threshold_value" {
  default = "5"
}

####################### nacl ######################################
variable "nacl_vpc_id" {
  default     = "vpc_id"
  description = "description"
}

variable "protocol" {
  default     = "tcp"
  description = "description"
}

variable "rule_no" {
  type        = number
  description = "description"
}

variable "action" {
  default     = ""
  description = "description"
}

variable "nacl_cidr_block" {
  default     = ""
  description = "description"
}

variable "from_port" {
  type        = number
  description = "description"
}

variable "to_port" {
  type        = number
  description = "description"
}

variable "nacl_tag" {
  default     = ""
  description = "description"
}
