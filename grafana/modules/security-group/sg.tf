resource "aws_security_group" "sg_bastion" {
  vpc_id      = var.bastion_vpc_id
  name        = var.bastion_sg_name
  dynamic "ingress" {
    for_each = var.inbound_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = var.ingr_protocol
      cidr_blocks = var.ingr_cidr_block
    }
  }
   egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "sg_prometheus" {
  vpc_id      = var.prometheus_vpc_id
  name        = var.prometheus_sg_name

  dynamic "ingress" {
    for_each = var.prometheus_inbound_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = var.prometheus_ingr_protocol
      cidr_blocks = var.prometheus_ingr_cidr_block
    }
  }
   egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "sg_grafana" {
  vpc_id      = var.grafana_vpc_id
  name        = var.grafana_sg_name

  dynamic "ingress" {
    for_each = var.grafana_inbound_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = var.grafana_ingr_protocol
      cidr_blocks = var.grafana_ingr_cidr_block
    }
  }
   egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
