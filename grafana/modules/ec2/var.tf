variable "bastion_sg_id" {
description = "specify the bastion sg id"
default = "bastion_sg_id"
}

variable "bastion_instance_type" {
    description = "specify the bastion instance type here"
    type = string
}
variable "bastion_tags" {
    description = "specify the bastion instance tag here"
    type = string
}
variable "bastion_subnet_id" {
    description = "specify public subnet id here"
    type = string
    default = "subnet id"
}
variable "key_name" {
    description = "specify key name here"
    type = string
}
variable "prometheus_instance_type" {
    description = "specify prometheus instance types here"
    type = string
}
variable "prometheus_subnet_id" {
    description = "specify prometheus subnet ids here"
}
variable "prometheus_server_tags" {
    description = "specify grafana server tags here"
    type = string
}
variable "grafana_instance_type" {
    description = "specify grafana instance type here"
    type = string
}
variable "grafana_subnet_id" {
    description = "specify grafana subnet id here"
    type = list(string)
}
variable "grafana_server_tags" {
    description = "specify grafana server name"
    type = list(string)
}
variable "prometheus_sg_id" {
    description = "specify prometheus security group id here"
    type = string
}
variable "grafana_sg_id" {
    description = "specify grafana security group id here"
    type = string
    default = "grafana_id"
}
