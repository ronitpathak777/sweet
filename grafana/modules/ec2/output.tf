output "ubuntu_ami" {
    value = data.aws_ami.ubuntu
}
output "bastion_server_id" {
    value = aws_instance.bastion_server.id
}
output "prometheus_instance_id" {
    value = aws_instance.prometheus_server.id
}
output "grafana_instance_id" {
    value = aws_instance.grafana_server[*].id
}
